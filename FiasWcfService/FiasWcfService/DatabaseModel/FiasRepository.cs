﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using FiasWcfService.ServiceModel;

namespace FiasWcfService.DatabaseModel
{
    public class FiasRepository
    {
        String connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        
        public AddressObject SearchByAddressObjectId(Guid addressObjectId)
        {
            AddressObject ao = new AddressObject();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                String query = "exec dbo.uspSelectAddressObjects @addressObjectId"; 

                ao = db.Query<AddressObject>(query , new { addressObjectId })
                    .FirstOrDefault<AddressObject>();
            }
            return ao;
        }       

        public AddressObject SearchByHistoryId(Guid historyObjectId)
        {
            AddressObject addressObject = new AddressObject();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                String query = "exec dbo.uspSelectHistoryObject @historyObjectId";

                addressObject =
                    db.Query<AddressObject>(query, new { historyObjectId })
                    .FirstOrDefault<AddressObject>();
            }
            return addressObject;
        }

        public List<AddressObject> SearchChildAddressObject(Guid parentAddressObjectId)
        {
            List<AddressObject> listAddressObject = new List<AddressObject>();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                String query = "exec dbo.uspSelectChildAddressObjects @parentAddressObjectId";

                listAddressObject =
                    db.Query<AddressObject>(query, new { parentAddressObjectId })
                    .ToList();
            }
            return listAddressObject;
        }

        public List<AddressHelper> SearchParentsAddresses(Guid addressObjectId)
        {
            List<AddressHelper> list = new List<AddressHelper>();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                String query = "exec dbo.uspSelectParents @addressObjectId";

                list = db.Query<AddressHelper>(query, new { addressObjectId })
                    .ToList<AddressHelper>();
            }
            return list;
        }
    }
}