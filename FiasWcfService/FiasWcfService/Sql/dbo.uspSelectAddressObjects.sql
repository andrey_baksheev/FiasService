USE AddressRegistry
GO
IF OBJECT_ID ( 'dbo.uspSelectAddressObjects', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.uspSelectAddressObjects
GO
CREATE PROCEDURE [dbo].[uspSelectAddressObjects]
(
	@addressObjectId UNIQUEIDENTIFIER	
)         
AS		 
BEGIN
	SET NOCOUNT ON
	SELECT AOGUID as ObjectGuid
			, AOID as HistoryGuid			
			, OFFNAME as FullName
			, OKATO as Okato
			, OKTMO as Oktmo
			, POSTALCODE as PostalCode
			, '' as FullAddress
			, UPDATEDATE as Updated
	FROM ADDROB
	WHERE AOGUID = @addressObjectId
		AND LIVESTATUS = 1
		AND CURRSTATUS = 0
END
GO

