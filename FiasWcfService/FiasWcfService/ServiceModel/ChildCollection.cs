﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FiasWcfService.ServiceModel
{
    [DataContract]
    [Serializable]
    public class ChildCollection
    {
        [DataMember(Name = "children", Order = 1)]      
        public List<ChildrenResult> list = new List<ChildrenResult>();
       
        public void AddRange(IEnumerable<ChildrenResult> children)
        {
            list.AddRange(children);
        }
    }
}
