﻿using System;
using System.Collections.Generic;

public class AddressObject
{
    public Guid AddressObjectId { get; set; }
    public Guid HistoryObjectId { get; set; }
    public Guid ParentObjectId { get; set; }
    public String Name { get; set; }
    public List<AddressObject> ChildAddressObjects { get; set; }

    public void Add(AddressObject child)
    {
        ChildAddressObjects.Add(child);
    }
}

public class AddressObject<T>: AddressObject
{
    public new List<T> ChildAddressObjects { get; set; }

    public void Add(T child)
    {
        ChildAddressObjects.Add(child);
    }
}

