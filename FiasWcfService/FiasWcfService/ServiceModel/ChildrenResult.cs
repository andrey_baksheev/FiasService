﻿using System;
using System.Runtime.Serialization;

namespace FiasWcfService.ServiceModel
{
    [DataContract]
    [Serializable]
    public class ChildrenResult
    {
        [DataMember(Name = "guid", Order = 1)] public Guid Guid { get; set; }
        [DataMember(Name = "name", Order = 2)] public String Name { get; set; }

        public ChildrenResult() { }
        public ChildrenResult(Guid guid, String name)
        {
            this.Guid = guid;
            this.Name = name;
        }
    }
}
