﻿using System;
using System.Web.Services;

[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[WebService(Description = "FiasService", Namespace = XmlNamespace)]
public class FiasAsmxService : WebService
{
    public const string XmlNamespace = "http://fias.nalog.ru/";

    [WebMethod(Description = "Get description of service")]
    public String GetDescription()
    {
       return "GetDescription";
    }

    /*[WebMethod(Description = "Get address object from FIAS")]
    public AddressObject GetAddressObject()
    {
        AddressObject addressObject = new AddressObject()
        {
            AddressObjectId = Guid.NewGuid(),
            HistoryObjectId = Guid.NewGuid(),
            ParentObjectId = Guid.NewGuid()
        };
        return addressObject;
    }*/
    [WebMethod(Description = "Get address object from FIAS")]
    public AddressObjectResult GetAddressObject()
    {
        AddressObject addressObject = new AddressObject()
        {
            AddressObjectId = Guid.NewGuid(),
            HistoryObjectId = Guid.NewGuid(),
            ParentObjectId = Guid.NewGuid()
        };

        AddressObjectResult result = new AddressObjectResult()
        {
            Result = addressObject
        };

        return result;
    }
}
