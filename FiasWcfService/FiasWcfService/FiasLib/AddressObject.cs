﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Runtime.Serialization;

[DataContract]
[Serializable]
public class AddressObject
{
    private const String formatDate = "yyyy-MM-dd";    
       
    [IgnoreDataMember]
    public Guid ObjectGuid { get; set; }

    [DataMember(Name = "guid", Order = 1)]
    public String ObjectGuidString { get; set; }

    [IgnoreDataMember]
    public Guid HistoryGuid { get; set; }

    [DataMember(Name = "historyGuid", Order = 2)]
    public String HistoryGuidString { get; set; }

    [DataMember(Name = "fullName", Order = 3)]
    public String FullName { get; set; }

    [DataMember(Name = "okato", Order = 4)]
    public String Okato { get; set; }

    [DataMember(Name = "oktmo", Order = 5)]
    public String Oktmo { get; set; }

    [DataMember(Name = "postalCode", Order = 6)]
    public String PostalCode { get; set; }

    [DataMember(Name = "fullAddress", Order = 7)]
    public String FullAddress { get; set; }

    [IgnoreDataMember]
    public DateTime? Updated { get; set; }

    [DataMember(Name = "updated", Order = 8)]
    private String UpdateDateString { get; set; }

    [OnSerializing]
    void OnSerializing(StreamingContext context)
    {
        ObjectGuidString = SerializeGuid(ObjectGuid);
        HistoryGuidString = SerializeGuid(HistoryGuid);
        SerializeUpdatedDate();
    }

    [OnDeserialized]
    void OnDeserializing(StreamingContext context)
    {
        ObjectGuid = DeserializeGuid(ObjectGuidString);
        HistoryGuid = DeserializeGuid(HistoryGuidString);
        DeserializeUpdatedDate();
    }

    private void SerializeUpdatedDate()
    {
        if (this.Updated.HasValue)
        {
            this.UpdateDateString = this.Updated.Value.ToString(formatDate);
        }
        else
        {
            this.UpdateDateString = "";
        }
    }
    private void DeserializeUpdatedDate()
    {
        if (String.IsNullOrWhiteSpace(this.UpdateDateString))
        {
            this.Updated = null;
        }
        else
        {
            this.Updated = DateTime.ParseExact(this.UpdateDateString, formatDate, CultureInfo.InvariantCulture);
        }
    }

    private String SerializeGuid(Guid guid)
    {
        return guid.ToString().ToUpper();
    }
    private Guid DeserializeGuid(String guidStr)
    {
        if (String.IsNullOrWhiteSpace(guidStr))
        {
            throw new ArgumentException("guid is empty");
        }

        Guid tmp;
        if (!Guid.TryParse(guidStr, out tmp))
        {
            throw new ArgumentException("invalid guid");
        }

        return tmp;
    }
}