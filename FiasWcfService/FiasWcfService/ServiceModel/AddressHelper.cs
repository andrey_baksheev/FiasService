﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiasWcfService.ServiceModel
{
    public class AddressHelper
    {
        public Decimal Level { get; set; }
        public String Name { get; set; }
        public String Unit { get; set; }
        public String FullName
        { 
         get { return  String.Format("{0} {1}", Name, Unit);  }
        }

        public static String CreateAddressString(IEnumerable<AddressHelper> list)
        {
            StringBuilder sb = new StringBuilder();
            Int32 cnt = list.Count();
            List<AddressHelper> parts = list.Take(cnt - 1).ToList();
            parts.ForEach(p=>sb.AppendFormat("{0}, ", p.FullName));
            sb.Append(list.Last().FullName);
            return sb.ToString();
        }        
    }
}
