﻿using System;
using System.IO;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using FiasWcfService.ServiceModel;
using FiasWcfService.DatabaseModel;

namespace FiasWcfService
{
    public class FiasService : IFiasService
    {
        static FiasRepository repo = new FiasRepository();

        public static void Configure(ServiceConfiguration config)
        {
            String serviceAddressString = ConfigurationManager.AppSettings["ServiceURI"];
            CheckServiceUri(serviceAddressString);
            Uri serviceUri = new Uri(serviceAddressString);
            Binding binding = new WebHttpBinding(WebHttpSecurityMode.None);
            ServiceEndpoint serviceEndpoint = new ServiceEndpoint(ContractDescription.GetContract(typeof(FiasWcfService.IFiasService)),
                                                                binding,
                                                                new EndpointAddress(serviceUri));
            WebHttpBehavior endpointBehavior = new WebHttpBehavior();
            endpointBehavior.DefaultOutgoingResponseFormat = WebMessageFormat.Json;
            endpointBehavior.DefaultOutgoingRequestFormat = WebMessageFormat.Json;
            endpointBehavior.DefaultBodyStyle = WebMessageBodyStyle.Wrapped;
            endpointBehavior.HelpEnabled = true;
            serviceEndpoint.EndpointBehaviors.Add(endpointBehavior);

            config.AddServiceEndpoint(serviceEndpoint);
        }

        private static void CheckServiceUri(String serviceAddressString)
        {
            if (String.IsNullOrEmpty(serviceAddressString))
            {
                throw new ArgumentException("Invalid ServiceURI");
            }
        }

        public Message GetHistoryAddressObject(Message msg)
        {
            Message response = null;
            MessageProperties properties = msg.Properties;
            Object tmp;
            HttpRequestMessageProperty prop = null;
            String queryString = String.Empty;
            if (properties.TryGetValue("httpRequest", out tmp))
            {
                prop = (HttpRequestMessageProperty)tmp;
                queryString = GetQueryString(prop);
                RequestQuery requestQuery = CreateRequestQuery(queryString);
                response = GetHistoryAddressObject(requestQuery);
            }
            return response;
        }        

        private Message GetHistoryAddressObject(RequestQuery requestQuery)
        {
            return CreateMessage(GetHistoryAddressObjectInfo(requestQuery.Guid));
        }

        private AddressObject GetHistoryAddressObjectInfo(Guid historyId)
        {
            return repo.SearchByHistoryId(historyId);
        }

        public Message GetAddressObject(Message msg)
        {
            Message response = null;
            MessageProperties properties = msg.Properties;
            Object tmp;
            HttpRequestMessageProperty prop = null;
            String queryString = String.Empty;
            if (properties.TryGetValue("httpRequest", out tmp))
            {
                prop = (HttpRequestMessageProperty)tmp;
                queryString = GetQueryString(prop);
                RequestQuery requestQuery = CreateRequestQuery(queryString);
                response = GetAddressObject(requestQuery);
            }
            return response;
        }

        private Message GetAddressObject(RequestQuery requestQuery)
        {
            AddressObject objectResult = GetAddressObjectInfo(requestQuery.Guid);
            return CreateMessage(objectResult);
        }

        private AddressObject GetAddressObjectInfo(Guid addressObjectId)
        {
            AddressObject ao = new AddressObject();
            ao = repo.SearchByAddressObjectId(addressObjectId);
            String addressString = AddressHelper.CreateAddressString(repo.SearchParentsAddresses(addressObjectId).ToList<AddressHelper>());           
            ao.FullAddress = addressString;          
            return ao;
        }

        private void CheckMethod(HttpRequestMessageProperty property, String pattern)
        {
            if (!property.Method.Equals(pattern))
            {
                throw new ArgumentException("Invalid HTTP method");
            }
        }

        private String GetQueryString(HttpRequestMessageProperty property)
        {
            return property.QueryString;
        }

        private NameValueCollection ParseQuery(String queryString)
        {
            return HttpUtility.ParseQueryString(queryString);
        }

        private RequestQuery CreateRequestQuery(String queryString)
        {
            RequestQuery requestQuery = new RequestQuery();
            NameValueCollection collection = ParseQuery(queryString);
            if (collection.AllKeys.Contains("guid"))
            {
                requestQuery = RequestQuery.CreateRequestQuery(collection.GetValues("guid")[0]);
            }
            else
            {
                throw new ArgumentException("Invalid query");
            }
            return requestQuery;
        }

        public Message GetChildAddressObject(Message msg)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            IncomingWebRequestContext incomingCtx = ctx.IncomingRequest;

            RequestQuery requestQuery = null;
            DataContractJsonSerializer formatter = new DataContractJsonSerializer(typeof(String));
            Message result = null;
            if (incomingCtx.Method != "POST")
            {
                return result;
            }

            requestQuery = EncodeQuery<RequestQuery>(msg);            

            if (requestQuery != null)
            {
                List<ChildrenResult> childrens = new List<ChildrenResult>();
                childrens = SelectConvertQueryToListChilds(requestQuery);                
                ChildCollection child = new ChildCollection();
                child.AddRange(childrens);
                result = CreateMessage(child);                              
            }

            return result;
        }

        private T EncodeQuery<T>(Message msg)
        {
            T req = default(T);

            using (var reader = msg.GetReaderAtBodyContents())
            {
                reader.Read();
                if (reader.HasValue)
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(reader.Value);

                    DataContractJsonSerializer formatter = new DataContractJsonSerializer(typeof(T));
                    using (var stream = new MemoryStream(base64EncodedBytes))
                    {
                        req = (T)formatter.ReadObject(stream);
                    }
                }
            }

            return req;
        }

        private List<ChildrenResult> SelectConvertQueryToListChilds(RequestQuery requestQuery)
        {
            List<ChildrenResult> childrens = new List<ChildrenResult>();
            childrens = repo.SearchChildAddressObject(requestQuery.Guid)
                        .Select<AddressObject, ChildrenResult>(s => new ChildrenResult
                        {
                            Guid = s.ObjectGuid,
                            Name = s.FullName
                        })
                        .ToList();

            return childrens;
        }

        Message CreateMessage<T>(T obj)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            return ctx.CreateJsonResponse<T>(obj);
        }               
    }
}
