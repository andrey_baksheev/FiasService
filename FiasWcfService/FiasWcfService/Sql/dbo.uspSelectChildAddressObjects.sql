USE AddressRegistry
GO
IF OBJECT_ID ( 'dbo.uspSelectChildAddressObjects', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.uspSelectChildAddressObjects
GO
CREATE PROCEDURE [dbo].[uspSelectChildAddressObjects]
(
	@parentAddressObjectId UNIQUEIDENTIFIER	
)         
AS		 
BEGIN
	SET NOCOUNT ON
	SELECT AOGUID as ObjectGuid
			, AOID as HistoryGuid			
			, OFFNAME as FullName
			, OKATO as Okato
			, OKTMO as Oktmo
			, POSTALCODE as PostalCode
			, '' as FullAddress
			, UPDATEDATE as Updated
	FROM ADDROB
	WHERE PARENTGUID = @parentAddressObjectId
		AND LIVESTATUS = 1
		AND CURRSTATUS = 0
END
GO