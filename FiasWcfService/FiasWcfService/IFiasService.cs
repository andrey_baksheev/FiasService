﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Channels;

namespace FiasWcfService
{
    [ServiceContract]
    public interface IFiasService
    {
      [OperationContract]
      [WebGet(UriTemplate = "history")]
      Message GetHistoryAddressObject(Message msg);

      [OperationContract]
      [WebGet(UriTemplate = "object")]
      Message GetAddressObject(Message msg);

      [OperationContract]
      [WebInvoke(Method = "POST", UriTemplate = "children")]
      Message GetChildAddressObject(Message msg);
    }
}

