USE AddressRegistry
GO
IF OBJECT_ID ( 'dbo.uspSelectHistoryObject', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.uspSelectHistoryObject
GO
CREATE PROCEDURE [dbo].[uspSelectHistoryObject]
(
	@historyObjectId UNIQUEIDENTIFIER	
)         
AS		 
BEGIN
	SET NOCOUNT ON
	SELECT AOGUID as ObjectGuid
			, AOID as HistoryGuid			
			, OFFNAME as FullName
			, OKATO as Okato
			, OKTMO as Oktmo
			, POSTALCODE as PostalCode
			, '' as FullAddress
			, UPDATEDATE as Updated
	FROM ADDROB
	WHERE AOID = @historyObjectId
END
GO
