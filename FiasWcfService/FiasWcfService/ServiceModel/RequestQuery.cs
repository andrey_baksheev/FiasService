﻿using System;
using System.Runtime.Serialization;

namespace FiasWcfService.ServiceModel
{
    [DataContract]
    [Serializable]
    public class RequestQuery
    {
        [DataMember(Name = "guid", Order = 1)]
        public Guid Guid { get; set; }

        public static RequestQuery CreateRequestQuery(String queryString)
        {
            Guid guid;
            RequestQuery requestQuery = null;
            if (Guid.TryParse(queryString, out guid))
            {
                requestQuery = new RequestQuery();
                requestQuery.Guid = guid;
            }
            else
            {
                throw new ArgumentException("Invalid argument", "queryString");
            }

            return requestQuery;            
        }


    }
}
